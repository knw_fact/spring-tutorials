package com.knowfact.springdocker.employee.service;

import com.knowfact.springdocker.employee.dao.EmployeeDao;
import com.knowfact.springdocker.employee.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EmployeeService {
    @Autowired
    EmployeeDao employeeDao;

    public Employee save(Employee employee){
        return employeeDao.save(employee);
    }

    public Optional<Employee> getEmployee(String id){
        return employeeDao.getEmployee(id);
    }
}
