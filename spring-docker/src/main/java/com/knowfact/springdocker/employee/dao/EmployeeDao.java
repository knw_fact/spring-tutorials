package com.knowfact.springdocker.employee.dao;

import com.knowfact.springdocker.employee.model.Employee;
import com.knowfact.springdocker.employee.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Repository
public class EmployeeDao {
    @Autowired
    EmployeeRepository employeeRepository;

    public Employee save(Employee employee){
        return employeeRepository.save(employee);
    }

    public Optional<Employee> getEmployee(String id){
        return employeeRepository.findById(id);
    }
}
