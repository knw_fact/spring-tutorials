package com.knowfact.springdocker.employee.controller;

import com.knowfact.springdocker.employee.model.Employee;
import com.knowfact.springdocker.employee.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class EmployeeController {
    @Autowired
    EmployeeService employeeService;

    @RequestMapping("/employee")
    ResponseEntity<Employee> setEmployee(@Valid @RequestBody Employee employee){
        return new ResponseEntity<Employee>(employeeService.save(employee), HttpStatus.OK);
    }

    @RequestMapping("/employee/{empid}")
    ResponseEntity<Optional<Employee>> getEmployee(@PathVariable("empid") String empid){
        return new ResponseEntity<Optional<Employee>>(employeeService.getEmployee(empid), HttpStatus.OK);
    }
}
